﻿using Assets.Types;

// ReSharper disable once CheckNamespace
namespace Pathfinding
{

    public class FsmPath : FsmTypeWrapper<Path>
    {
        public FsmPath(Path value) : base(value)
        {
        }
    }
}