﻿using UnityEngine;

namespace Assets.Types
{
    public class FsmTypeWrapper<T> : Object where T : class
    {
        public T Value;

        public FsmTypeWrapper(T value)
        {
            Value = value;
        }
    }
}
