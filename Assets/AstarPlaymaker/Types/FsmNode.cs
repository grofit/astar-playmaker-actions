﻿using Assets.Types;

// ReSharper disable once CheckNamespace
namespace Pathfinding
{
    public class FsmNode : FsmTypeWrapper<GraphNode>
    {
        public FsmNode(GraphNode value)
            : base(value)
        {}
    }
}