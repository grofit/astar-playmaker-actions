﻿using Assets.Types;

namespace Pathfinding
{
    public class FsmGraph : FsmTypeWrapper<NavGraph>
    {
        public FsmGraph(NavGraph value)
            : base(value)
        { }
    }
}