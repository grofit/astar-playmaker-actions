﻿using Pathfinding;
using UnityEngine;

namespace Assets.AstarPlaymaker.Extensions
{
    public static class GameObjectExtensions
    {
        public static void CreatePathTo(this GameObject gameObject, Vector3 destination, OnPathDelegate callback)
        {
            var startPosition = gameObject.transform.position;
            CreatePathTo(gameObject, startPosition, destination, callback);
        }

        public static void CreatePathTo(this GameObject gameObject, Vector3 startingPoint, Vector3 destination, OnPathDelegate callback)
        {
            var seeker = gameObject.GetComponent<Seeker>();
            if (seeker == null)
            { CreatePathUsingABPath(startingPoint, destination, callback); }
            else
            { CreatePathUsingSeeker(seeker, startingPoint, destination, callback); }
        }

        private static void CreatePathUsingABPath(Vector3 startPosition, Vector3 endPosition, OnPathDelegate callback)
        {
            var pathPlaceholder = ABPath.Construct(startPosition, endPosition, callback);
            AstarPath.StartPath(pathPlaceholder);
        }

        private static void CreatePathUsingSeeker(Seeker seeker, Vector3 startPosition, Vector3 endPosition, OnPathDelegate callback)
        {
            seeker.StartPath(startPosition, endPosition, callback);
        }
    }
}