﻿using UnityEngine;

namespace Assets.AstarPlaymaker.Extensions
{
    public static class Vector3Extensions
    {
        public static Vector3 CalculateRelativeMovement(this Vector3 currentPosition, Vector3 destination, float movementSpeed)
        {
            var movementDifference = destination - currentPosition;
            var movementDirection = movementDifference.normalized * movementSpeed * Time.deltaTime;
            var isDirectionLessThanDifference = movementDirection.sqrMagnitude < movementDifference.sqrMagnitude;
            return isDirectionLessThanDifference ? movementDirection : movementDifference;
        }

        public static bool IsCloseEnoughTo(this Vector3 currentPosition, Vector3 destination, float distanceTolerance)
        {
            return Vector3.Distance(destination, currentPosition) <= distanceTolerance;
        }
    }
}