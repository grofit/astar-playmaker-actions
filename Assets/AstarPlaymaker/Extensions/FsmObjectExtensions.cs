﻿using HutongGames.PlayMaker;
using UnityEngine;

namespace Assets.AstarPlaymaker.Extensions
{
    public static class FsmObjectExtensions
    {
        public static T GetTrueType<T>(this FsmObject fsmObject)
            where T : Object
        {
            return (T)fsmObject.Value;
        }
    }
}