﻿using HutongGames.PlayMaker;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Movement
{
    [ActionCategory("A* Pathfinder")]
    public class HasReachedDestinationInTime : FsmStateAction
    {
        [RequiredField] 
        [Tooltip("The object to move along the path.")] 
        public FsmOwnerDefault sourceGameObject;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The destination position")]
        public FsmVector3 destination;

        [Tooltip("The tolerance on the distance from destination")]
        public FsmFloat distanceTolerance;

        [RequiredField]
        [UIHint(UIHint.FsmEvent)] 
        [Tooltip("An event for when another waypoint has been reached")] 
        public FsmEvent OnReachedDestination;

        [RequiredField]
        [UIHint(UIHint.FsmEvent)] 
        [Tooltip("An event for if source was unable to reach destination")] 
        public FsmEvent OnUnableToReachDestination;

        [Tooltip("The timeout period to wait for it to reach destination")]
        public FsmInt timeoutPeriodInMilliseconds;

        private int elapsedTimeInMilliseconds;

        public override void Reset()
        {
            elapsedTimeInMilliseconds = 0;
            distanceTolerance = 0.5f;
            timeoutPeriodInMilliseconds = 2000;
        }

        public override void OnUpdate()
        {
            elapsedTimeInMilliseconds += (int)(Time.deltaTime * 1000);
            var sourceObject = Fsm.GetOwnerDefaultTarget(sourceGameObject);

            if (Vector3.Distance(destination.Value, sourceObject.transform.position) <= distanceTolerance.Value)
            {
                Fsm.Event(OnReachedDestination);
                return;
            }

            if (elapsedTimeInMilliseconds >= timeoutPeriodInMilliseconds.Value)
            { Fsm.Event(OnUnableToReachDestination); }
        }
    }
}
