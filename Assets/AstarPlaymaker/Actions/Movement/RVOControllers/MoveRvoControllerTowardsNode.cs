using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Movement.RVOControllers
{
    [ActionCategory("RVOController")]
    public class MoveRvoControllerTowardsNode : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The object with character controller to move")]
        public FsmOwnerDefault sourceGameObject;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmNode))]
        [Tooltip("The destination node")]
        public FsmObject destinationNode;

        [Tooltip("The movement speed")]
        public FsmFloat movementSpeed;

        [Tooltip("The tolerance on the distance from destination")]
        public FsmFloat distanceTolerance;

        [Tooltip("Ignore the Y component when moving")]
        public FsmBool ignoreY;

        private GameObject sourceObject;
        private RVOController rvoController;

        public override void Reset()
        {
            distanceTolerance = 0.5f;
            movementSpeed = 1.0f;
            rvoController = null;
            ignoreY = true;
        }

        public override void OnEnter()
        {
            sourceObject = Fsm.GetOwnerDefaultTarget(sourceGameObject);
            rvoController = sourceObject.GetComponent<RVOController>();
        }

        public override void OnUpdate()
        {
            var destination = (Vector3)destinationNode.GetTrueType<FsmNode>().Value.position;
            
            if (ignoreY.Value)
            { destination = new Vector3(destination.x, sourceObject.transform.position.y, destination.z); }

            if (Vector3.Distance(destination, sourceObject.transform.position) <= distanceTolerance.Value)
            {
                Finish();
                return;
            }

            var movementVector = sourceObject.transform.position.CalculateRelativeMovement(destination, movementSpeed.Value);
            rvoController.Move(movementVector);
        }
    }
}