﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Movement.CharacterControllers
{
    [ActionCategory(ActionCategory.CharacterController)]
    public class MoveCharacterControllerTowardsPosition : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The object with character controller to move")]
        public FsmOwnerDefault sourceGameObject;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The destination position")]
        public FsmVector3 destination;

        [Tooltip("The movement speed")]
        public FsmFloat movementSpeed;

        [Tooltip("The tolerance on the distance from destination")]
        public FsmFloat distanceTolerance;

        [Tooltip("Ignore the Y component when moving")]
        public FsmBool ignoreY;

        private GameObject sourceObject;
        private CharacterController characterController;

        public override void Reset()
        {
            distanceTolerance = 0.5f;
            movementSpeed = 1.0f;
            characterController = null;
            ignoreY = true;
        }

        public override void OnEnter()
        {
            sourceObject = Fsm.GetOwnerDefaultTarget(sourceGameObject);
            characterController = sourceObject.GetComponent<CharacterController>();
        }

        public override void OnUpdate()
        {
            if(ignoreY.Value)
            { destination.Value = new Vector3(destination.Value.x, sourceObject.transform.position.y, destination.Value.z); }

            if (Vector3.Distance(destination.Value, sourceObject.transform.position) <= distanceTolerance.Value)
            {
                Finish();
                return;
            }

            var movementVector = sourceObject.transform.position.CalculateRelativeMovement(destination.Value, movementSpeed.Value);
            characterController.Move(movementVector);
        }
    }
}