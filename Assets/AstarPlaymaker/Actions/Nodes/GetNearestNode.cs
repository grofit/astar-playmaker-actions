﻿using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Nodes
{
    public class GetNearestNode : FsmStateAction
    {
        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The position to check")]
        FsmVector3 position;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmNode))]
        FsmObject outputNode;

        [UIHint(UIHint.FsmEvent)]
        [Tooltip("The event raised if no node was close enough")]
        public FsmEvent OnGetNodeFailed;

        [UIHint(UIHint.FsmEvent)]
        [Tooltip("The event raised if a node was found")]
        public FsmEvent OnGetNodeSuccess;

        public override void OnEnter()
        {
            var closestResult = AstarPath.active.GetNearest(position.Value);
            if (closestResult.node == null)
            {
                FailedToGetNode();
                return;
            }
            FoundNode(closestResult.node);
        }

        private void FailedToGetNode()
        {
            if (OnGetNodeFailed == null)
            {
                Finish();
                return;
            }
            Fsm.Event(OnGetNodeFailed);
        }

        private void FoundNode(GraphNode closestNode)
        {
            outputNode.Value = new FsmNode(closestNode);
            if (OnGetNodeSuccess == null)
            {
                Finish();
                return;
            }
            Fsm.Event(OnGetNodeSuccess);
        }
    }
}