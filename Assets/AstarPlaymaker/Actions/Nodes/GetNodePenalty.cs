﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Nodes
{
    public class GetNodePenalty : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The node to check against")]
        [ObjectType(typeof(FsmNode))]
        public FsmObject nodeToCheck;

        [RequiredField]
        [Tooltip("The output variable indicating a true or a false if it is passible")]
        [UIHint(UIHint.Variable)]
        public FsmInt nodePenaltyOutput;

        public override void OnEnter()
        {
            var fsmNode = nodeToCheck.GetTrueType<FsmNode>();
            nodePenaltyOutput.Value = (int)fsmNode.Value.Penalty;
            Finish();
        }
    }
}