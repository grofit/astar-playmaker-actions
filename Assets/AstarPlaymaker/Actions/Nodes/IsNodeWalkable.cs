﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Nodes
{
    public class IsNodeWalkable : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The node to check against")]
        [ObjectType(typeof(FsmNode))]
        public FsmObject nodeToCheck;

        [Tooltip("The output variable indicating a true or a false if it is passible")]
        [UIHint(UIHint.Variable)]
        public FsmBool isNodeWalkableOutput;

        [Tooltip("Event to notify of the node being accessible")]
        [UIHint(UIHint.FsmEvent)]
        public FsmEvent onNodeIsAccessible;
        
        [Tooltip("Event to notify of the node not being accessible")]
        [UIHint(UIHint.FsmEvent)]
        public FsmEvent onNodeIsNotAccessible;

        public override void OnEnter()
        {
            var fsmNode = nodeToCheck.GetTrueType<FsmNode>();
            var isWalkable = fsmNode.Value.Walkable;

            if (isNodeWalkableOutput.UsesVariable)
            { isNodeWalkableOutput.Value = isWalkable; }

            if (onNodeIsAccessible != null && isWalkable)
            {
                Fsm.Event(onNodeIsAccessible);
                return;
            }

            if (onNodeIsNotAccessible != null && !isWalkable)
            {
                Fsm.Event(onNodeIsNotAccessible);
                return;
            }

            Finish();
        }
    }
}