﻿using HutongGames.PlayMaker;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Other
{
    [ActionCategory("Spawners")]
    public class SpawnSingle : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The spawner object")]
        [UIHint(UIHint.FsmGameObject)]
        public FsmOwnerDefault spawnerObject;

        [RequiredField]
        [Tooltip("The object to spawn")]
        public FsmGameObject spawnEntity;

        public override void OnEnter()
        {
            var spawnerGameObject = Fsm.GetOwnerDefaultTarget(spawnerObject);
            Object.Instantiate(spawnEntity.Value, spawnerGameObject.transform.position, spawnerGameObject.transform.rotation);
            Finish();
        }
    }
}