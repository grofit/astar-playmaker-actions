﻿using HutongGames.PlayMaker;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Other
{
    [ActionCategory("Spawners")]
    public class SpawnFiniteAmount : FsmStateAction 
    {
        [RequiredField]
        [Tooltip("The spawner object")]
        [UIHint(UIHint.FsmGameObject)]
        public FsmOwnerDefault spawnerObject;

        [RequiredField]
        [Tooltip("The object to spawn")]
        public FsmGameObject spawnEntity;

        [RequiredField]
        [Tooltip("The amount to spawn")]
        public FsmInt spawnCount;

        private int spawned;
        private GameObject spawnerGameObject;

        public override void Reset()
        {
            spawned = 0;
            spawnCount = 50;
        }

        public override void OnEnter()
        {
            spawnerGameObject = Fsm.GetOwnerDefaultTarget(spawnerObject);
        }

        public override void OnUpdate()
        {
            if (spawned >= spawnCount.Value)
            {
                Finish();
                return;
            }

            Object.Instantiate(spawnEntity.Value, spawnerGameObject.transform.position,spawnerGameObject.transform.rotation);
            spawned++;
        }
    }
}
