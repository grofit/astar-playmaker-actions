﻿using HutongGames.PlayMaker;

namespace Assets.AstarPlaymaker.Actions.Graphs
{
    public class RescanGraphs : FsmStateAction
    {
        public override void OnEnter()
        {
            AstarPath.active.Scan();
            Finish();
        }
    }
}