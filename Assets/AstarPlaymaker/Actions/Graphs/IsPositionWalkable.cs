using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Graphs
{
    public class IsPositionWalkable : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The position to check")]
        public FsmVector3 positionToCheck;

        [Tooltip("The output variable indicating a true or a false if it is passible")]
        [UIHint(UIHint.Variable)]
        public FsmBool isPositionWalkableOutput;

        [Tooltip("Event to notify of the node being accessible")]
        [UIHint(UIHint.FsmEvent)]
        public FsmEvent onNodeIsAccessible;

        [Tooltip("Event to notify of the node not being accessible")]
        [UIHint(UIHint.FsmEvent)]
        public FsmEvent onNodeIsNotAccessible;

        public override void OnEnter()
        {
            var graphs = AstarPath.active.graphs;
            var isWalkable = false;
            foreach (var graph in graphs)
            {
                if(isGraphWalkable(graph))
                { isWalkable = true; }
            }

            if (isPositionWalkableOutput.UsesVariable)
            { isPositionWalkableOutput.Value = isWalkable; }

            if (onNodeIsAccessible != null && isWalkable)
            {
                Fsm.Event(onNodeIsAccessible);
                return;
            }

            if (onNodeIsNotAccessible != null && !isWalkable)
            {
                Fsm.Event(onNodeIsNotAccessible);
                return;
            }

            Finish();
        }

        private bool isGraphWalkable(NavGraph graph)
        {
            if (graph is RecastGraph)
            {
                // should be PointOnNavmesh
                var nearestNode = (graph as RecastGraph).GetNearest(positionToCheck.Value);
                if (nearestNode.node != null)
                { return nearestNode.node.walkable; }
            }
            else
            {
                var nearestNode = graph.GetNearest(positionToCheck.Value);
                if(nearestNode.node != null)
                { return nearestNode.node.walkable; }
            }

            return false;
        }
    }
}