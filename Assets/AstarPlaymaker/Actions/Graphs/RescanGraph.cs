﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Graphs
{
    public class RescanGraph : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The graph to use")]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmGraph))]
        public FsmObject graph;

        public override void OnEnter()
        {
            var fsmGraph = graph.GetTrueType<FsmGraph>();
            var underlyingGraph = fsmGraph.Value;
            underlyingGraph.ScanInternal();
            Finish();
        }
    }
}