﻿using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Graphs
{
    public class GetGraphFromIndex : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The index of the graph")]
        public FsmInt graphIndex;

        [RequiredField]
        [Tooltip("The graph output")]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmGraph))]
        public FsmObject graphOutput;

        public override void OnEnter()
        {
            var graph = AstarPath.active.graphs[graphIndex.Value];
            graphOutput.Value = new FsmGraph(graph);
            Finish();
        } 
    }
}