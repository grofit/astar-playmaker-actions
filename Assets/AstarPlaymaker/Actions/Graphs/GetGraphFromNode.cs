﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Graphs
{
    public class GetGraphFromNode : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The node to check against")]
        [ObjectType(typeof(FsmNode))]
        public FsmObject nodeToCheck;

        [RequiredField]
        [Tooltip("The graph output")]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmGraph))]
        public FsmObject graphOutput;

        public override void OnEnter()
        {
            var fsmNode = nodeToCheck.GetTrueType<FsmNode>();
            var nodeToUse = fsmNode.Value;
            var graph = AstarPath.active.graphs[nodeToUse.GraphIndex];
            graphOutput.Value = new FsmGraph(graph);
            Finish();
        }
    }
}