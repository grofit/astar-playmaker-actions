﻿using System.Collections.Generic;
using HutongGames.PlayMaker;
using Pathfinding;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Pathing
{
    [ActionCategory("A* Pathfinder")]
    public class DebugDrawPath : FsmStateAction
    {
        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmPath))]
        [Tooltip("The path to draw")]
        public FsmObject path;

        [Tooltip("The color of the line.")]
        public FsmColor color;

        private List<Vector3> vectorPath;

        public override void Reset()
        {
            color = Color.green;
            vectorPath = null;
        }

        public override void OnEnter()
        {
            var fsmPath = (path.Value as FsmPath);
            vectorPath = fsmPath.Value.vectorPath;
        }

        public override void OnUpdate()
        {
            if(vectorPath.Count <= 0) { return; }
            for (var i = 1; i < vectorPath.Count; i++)
            { Debug.DrawLine(vectorPath[i-1], vectorPath[i], color.Value); }
        }
    }
}