﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Pathing
{
    [ActionCategory("A* Pathfinder")]
    public class ReleasePathInPool : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The object bound to the path in the pool")]
        public FsmOwnerDefault sourceGameObject;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmPath))]
        [Tooltip("The path to use")]
        public FsmObject path;

        public override void OnEnter()
        {
            var gameObject = Fsm.GetOwnerDefaultTarget(sourceGameObject);
            var fsmPath = path.GetTrueType<FsmPath>();

            if (fsmPath != null && fsmPath.Value != null)
            {
                fsmPath.Value.Release(gameObject);
                path.Value = null;
            }
        }
    }
}