﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Pathing
{
    [ActionCategory("A* Pathfinder")]
    public class CreatePathToPosition : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The owner/source object.")]
        public FsmOwnerDefault sourceGameObject;

        [RequiredField]
        [Tooltip("The destination object")]
        public FsmVector3 destinationPosition;

        [RequiredField]
        [ObjectType(typeof(FsmPath))]
        [UIHint(UIHint.Variable)]
        [Tooltip("The variable to store the path")]
        public FsmObject outputFsmPath;

        [UIHint(UIHint.FsmEvent)]
        [Tooltip("The event raised if the path was created")]
        public FsmEvent OnPathSucceeded;

        [UIHint(UIHint.FsmEvent)]
        [Tooltip("The event raised if the path could not be created, this also sets the string event data to the error")]
        public FsmEvent OnPathFailed;

        public override void OnEnter()
        {
            var gameObject = Fsm.GetOwnerDefaultTarget(sourceGameObject);
            gameObject.CreatePathTo(destinationPosition.Value, PathFinishedDeletate);
        }

        private void PathFinishedDeletate(Path path)
        {
            outputFsmPath.Value = new FsmPath(path);

            if (path.error)
            { Fsm.EventData.StringData = path.errorLog; }
            Fsm.Event(path.error ? OnPathFailed : OnPathSucceeded);
        }
    }
}