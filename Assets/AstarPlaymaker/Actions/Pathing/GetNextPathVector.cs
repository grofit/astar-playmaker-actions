﻿using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Pathing
{
    [ActionCategory("A* Pathfinder")]
    public class GetNextPathVector : FsmStateAction
    {
        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmPath))]
        [Tooltip("The path to use")]
        public FsmObject pathToFollow;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The index of the path")]
        public FsmInt pathIndex;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The output vector")]
        public FsmVector3 outputVector;

        [RequiredField]
        [Tooltip("The event to send if no more nodes exist")]
        public FsmEvent OnPathFinished;

        public override void OnEnter()
        {
            var fsmPath = (pathToFollow.Value as FsmPath);

            if (pathIndex.Value < fsmPath.Value.vectorPath.Count)
            {
                outputVector.Value = fsmPath.Value.vectorPath[pathIndex.Value++];
                Finish();
            }
            else
            {
                pathIndex.Value = 1;
                Fsm.Event(OnPathFinished);
            }            
        }
    }
}