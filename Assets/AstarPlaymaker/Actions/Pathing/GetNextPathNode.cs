using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Pathing
{
    [ActionCategory("A* Pathfinder")]
    public class GetNextPathNode : FsmStateAction
    {
        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmPath))]
        [Tooltip("The path to use")]
        public FsmObject pathToFollow;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The index of the path")]
        public FsmInt pathIndex;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmNode))]
        [Tooltip("The output node")]
        public FsmObject outputNode;

        [RequiredField]
        [Tooltip("The event to send if no more nodes exist")]
        public FsmEvent OnPathFinished;

        public override void OnEnter()
        {
            var fsmPath = (pathToFollow.Value as FsmPath);

            if (pathIndex.Value < fsmPath.Value.path.Count)
            {
                outputNode.Value = new FsmNode(fsmPath.Value.path[pathIndex.Value++]);
                Finish();
            }
            else
            {
                pathIndex.Value = 1;
                Fsm.Event(OnPathFinished);
            }
        }
    }
}