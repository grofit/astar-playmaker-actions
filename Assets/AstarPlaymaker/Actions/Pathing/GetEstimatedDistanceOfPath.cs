﻿using Assets.AstarPlaymaker.Extensions;
using HutongGames.PlayMaker;
using Pathfinding;
using UnityEngine;
using Tooltip = HutongGames.PlayMaker.TooltipAttribute;

namespace Assets.AstarPlaymaker.Actions.Pathing
{
    [ActionCategory("A* Pathfinder")]
    public class GetEstimatedDistanceOfPath : FsmStateAction
    {
        [RequiredField]
        [UIHint(UIHint.Variable)]
        [ObjectType(typeof(FsmPath))]
        [Tooltip("The path to use")]
        public FsmObject path;

        [RequiredField]
        [UIHint(UIHint.Variable)]
        [Tooltip("The estimated traversal time")]
        public FsmFloat traversalTime;

        public override void OnEnter()
        {
            var fsmPath = path.GetTrueType<FsmPath>();
            if (fsmPath != null && fsmPath.Value != null)
            {
                var totalDistance = GetTotalDistanceOfPath(fsmPath.Value);
                traversalTime.Value = totalDistance;
            }
            Finish();
        }

        private float GetTotalDistanceOfPath(Path path)
        {
            var totalDistance = 0.0f;
            for (var i = 1;i < path.vectorPath.Count; i++)
            {
                var previousPosition = path.vectorPath[i - 1];
                var nextPosition = path.vectorPath[i];
                totalDistance = Vector3.Distance(previousPosition, nextPosition);
            }
            return totalDistance;
        }
    }
}