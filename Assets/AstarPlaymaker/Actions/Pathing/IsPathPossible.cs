﻿using HutongGames.PlayMaker;
using Pathfinding;

namespace Assets.AstarPlaymaker.Actions.Pathing
{
    public class IsPathPossible : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The start position of the path")]
        public FsmVector3 startPosition;

        [RequiredField]
        [Tooltip("The end position of the path")]
        public FsmVector3 endPosition;

        [UIHint(UIHint.Variable)]
        [Tooltip("The result with a true or false based upon if the path is possible")]
        public FsmBool isPathPossibleOutput;

        [UIHint(UIHint.FsmEvent)]
        [Tooltip("The event to indicate the path is possible")]
        public FsmEvent onPathIsPossibleEvent;

        [UIHint(UIHint.FsmEvent)]
        [Tooltip("The event to indicate the path is not possible")]
        public FsmEvent onPathIsNotPossibleEvent;

        public override void OnEnter()
        {
            var startNode = AstarPath.active.GetNearest(startPosition.Value).node;
            var destinationNode = AstarPath.active.GetNearest(endPosition.Value).node;
            var pathIsPossible = PathUtilities.IsPathPossible(startNode, destinationNode);

            if(isPathPossibleOutput.UsesVariable)
            { isPathPossibleOutput = pathIsPossible; }

            if(onPathIsPossibleEvent != null && isPathPossibleOutput.Value)
            { Fsm.Event(onPathIsPossibleEvent); }

            if (onPathIsPossibleEvent != null && pathIsPossible)
            {
                Fsm.Event(onPathIsPossibleEvent);
                return;
            }

            if (onPathIsNotPossibleEvent != null && !pathIsPossible)
            {
                Fsm.Event(onPathIsNotPossibleEvent);
                return;
            }
            
            Finish();
        }
    }
}