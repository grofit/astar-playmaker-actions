# Playmaker - A* Pathfinding Actions #

This is a simple set of A* pathfinding actions I wrote as the original set was not compatible with the latest version and was not actively maintained, which can be found here:

https://github.com/grofit/playmaker-astar-actions

## How do I install it? ##

Go in the dist folder and take the unity package, currently only A* pathfinding pro is supported.

## How do I use it? ##

You should now have a category in playmaker called **A* Pathfinder**, within there you should find most actions self explanatory but for the most common use cases you would use **CreatePathToObject** and save it to a variable, then **GetNextPathVector** and save that to a variable and then do your animation, movement at whatever speed you want (there are helper actions such as **MoveCharacterControllerTowardsPosition**) then once you are done with that vector repeat and get the next one until the **GetNextPathVector** invokes the **OnPathFinished** event, which should indicate to you that it has reached its destination.

There are a few examples although not all are complete.

## How do I develop it further? ##
First of all download your version of Playmaker and add it to the project, and also your version of A* pathfinder (free or pro), you can ignore the UnityVS stuff but if you have that too feel free to add that in.

Then just fix any issues you find or add some new actions, I wont merge any in without having a quick chat about the functionality but feel free to branch as much as you want.

## Developed With ##

A* Pathfinding - 3.4.0.6

Playmaker - 1.7.5